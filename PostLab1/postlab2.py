from sense_hat import SenseHat
from time import sleep
import numpy as np
import matplotlib.pyplot as plt

    

sense=SenseHat()


sense.clear() ## to clear the LED matrix

temperature=sense.get_temperature()
initTemp=round(temperature,1)  ## round temperature to 1 decimal place

temps = np.zeros(30)
print("collect starts")

for i in range(30):
    temps[i]= sense.get_temperature()
    sleep(1)

print("collect complete")

avg = np.array(temps)

for i in range (1,28):
    avg[i] = (avg[i-1]+avg[i+1]+avg[i])/3
    
    
plt.plot(range(30),temps,'g',label = 'original')
plt.plot(range(30),avg,'b',label = 'smoothed')
plt.legend()
plt.show()
    
sense.clear()
