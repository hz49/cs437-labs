
import numpy as np
import time
from picamera2 import Picamera2
from picamera2.encoders import H264Encoder
from picamera2.outputs import CircularOutput
from libcamera import controls
import cv2

from sense_hat import SenseHat
from time import sleep


sense=SenseHat()


sense.clear() ## to clear the LED matrix

picam2=Picamera2()  ## Create a camera object

dispW=1280
dispH=720
## Next, we configure the preview window size that determines how big should the image be from the camera, the bigger the image the more the details you capture but the slower it runs
## the smaller the size, the faster it can run and get more frames per second but the resolution will be lower. We keep 
picam2.preview_configuration.main.size= (dispW,dispH)  ## 1280 cols, 720 rows. Can also try smaller size of frame as (640,360) and the largest (1920,1080)
## with size (1280,720) you can get 30 frames per second

## since OpenCV requires RGB configuration we set the same format for picam2. The 888 implies # of bits on Red, Green and Blue
picam2.preview_configuration.main.format= "RGB888"
picam2.preview_configuration.align() ## aligns the size to the closest standard format
picam2.preview_configuration.controls.FrameRate=30 ## set the number of frames per second, this is set as a request, the actual time it takes for processing each frame and rendering a frame can be different

picam2.configure("preview")
## 3 types of configurations are possible: preview is for grabbing frames from picamera and showing them, video is for grabbing frames and recording and images for capturing still images.

faceCascade = cv2.CascadeClassifier("/home/pi/Downloads/haar.xml")
picam2.start()

temperature=sense.get_temperature()
initTemp=round(temperature,2)  ## round temperature to 1 decimal place
flag = True
while flag:
	temperature=sense.get_temperature()
	tmp=round(temperature,2)  ## round temperature to 1 decimal place
	#print(tmp-initTemp)
	if(abs(tmp - initTemp) >= 0.6):
		print("tmp change")
		while(True):
			frame=picam2.capture_array()
			cv2.imshow("Camera Frame", frame)
			time.sleep(0.2)
			key=cv2.waitKey(1) & 0xFF

			
			## frame[rows,columns] --> is the pixel of each frame
			
			## the above command will only grab the frame
			
			#cv2.imshow("piCamera2", frame) ## show the frame
			#key=cv2.waitKey(1) & 0xFF
			
			if key ==ord(" "):
				cv2.imwrite("frame-" + str(time.strftime("%H:%M:%S", time.localtime())) + ".jpg", frame)
			if key == ord("q"): ## stops for 1 ms to check if key Q is pressed
				flag = False
				break
			
		
