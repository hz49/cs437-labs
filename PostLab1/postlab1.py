from sense_hat import SenseHat
from time import sleep

sense=SenseHat()
sense.clear()
blue= (0,0,255)
yellow= (255,255,0)
red=(255,0,0)
x_val = 3
y_val= 5
sense.clear()
flag = 1
while flag:
    for event in sense.stick.get_events():
        #temperature=sense.get_temperature()
        #temperature=round(temperature,1)  ## round temperature to 1 decimal place
        #print(event.direction,event.action)
        if event.action =="pressed":  ## check if the joystick was pressed
            sense.clear()
            if event.direction=="middle":   ## to check for other directions use "up", "down", "left", "right"
                flag = 0
                break
            if event.direction=="up":
                y_val -=1
                y_val = max(0,y_val)
            if event.direction=="left":
                x_val -=1
                x_val = max(0,x_val)
            if event.direction=="down":
                y_val +=1
                y_val = min(y_val,7)
            if event.direction=="right":
                x_val +=1
                x_val = min(x_val,7)
                
        sense.set_pixel(x_val,y_val,(0,0,255))
        
sense.clear()

            
            
